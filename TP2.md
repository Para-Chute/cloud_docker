
  

# Compte rendu Jeleff Clément B3B Infra & Réseaux

  

  

# Rendu TP2

  

# I. Gestion de conteneurs Docker

  

  

>[root@localhost ~]# ps -ef

UID PID PPID C STIME TTY TIME CMD

root 1 0 0 11:22 ? 00:00:04 /usr/lib/systemd/systemd --system --deserialize 16

root 2 0 0 11:22 ? 00:00:00 [kthreadd]

root 3 2 0 11:22 ? 00:00:03 [ksoftirqd/0]

root 5 2 0 11:22 ? 00:00:00 [kworker/0:0H]

...]

root 5081 2 0 11:22 ? 00:00:00 [xfsaild/sda1]

root 13591 1 0 12:12 ? 00:00:00 /usr/sbin/crond -n

polkitd 13635 1 0 12:12 ? 00:00:00 /usr/lib/polkit-1/polkitd --no-debug

root 13648 1 0 12:12 ? 00:00:00 /usr/bin/python2 -Es /usr/sbin/tuned -l -P

root 36373 1 2 12:16 ? 00:00:01 /usr/bin/dockerd-current --add-runtime docker-runc=/usr/libexec/docker

root 36379 36373 0 12:16 ? 00:00:00 /usr/bin/docker-containerd-current -l unix:///var/run/docker/libcontai

root 36472 2 0 12:16 ? 00:00:00 [kworker/1:0]

root 36563 36379 0 12:17 ? 00:00:00 /usr/bin/docker-containerd-shim-current 804535c6ee624b51880e93a6bb9b1f

root 36580 36563 0 12:17 ? 00:00:00 sleep 9999

root 36609 7195 0 12:17 pts/0 00:00:00 ps -ef

  

* Avec la commande "ps -ef" nous avons accès à la liste de tous les processus lancé sur la machine.

  

  

Par exemple si l'on prends le dernier processus lancé sur la machine :

  

> "root 36609 7195 0 12:17 pts/0 00:00:00 ps -ef"

  

On se rends compte que c'est notre requête "ps -ef" et que son PID est de 36609 et que son ParentPID est 7195, c'est donc le service -bash qui à exécuter notre requête.

  

  

Une fois les bases acquises nous pouvons:

  

### 🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker

  

* Dans un premier temps nous allons chercher le processus docker.

  

>root 36373 1 2 12:16 ? 00:00:01 /usr/bin/dockerd-current --add-runtime docker-runc=/usr/libexec/docker

#### Le voila ! Il possède un PID de 36373 et un PPID de 1 ce qui veut dire qu'il à été exécuté par le systemd

  

* Passons maintenant au containerd, comme nous pouvons voir ci-dessous il à un PID de 36379 et son parent est: 36373 , parent qui est donc docker lui même !

  

>root 36379 36373 0 12:16 ? 00:00:00 /usr/bin/docker-containerd-current -l unix:///var/run/docker/libcontai

  

#### Pour finir, Container-shim.

#### Ce dernier à un PID de 36563 et son parent, 36379 est containerd

  

> root 36563 36379 0 12:17 ? 00:00:00 /usr/bin/docker-containerd-shim-current

  

  

* 🌞 Utiliser l'API HTTP mise à disposition par `dockerd`

* utiliser un `curl` (ou autre) pour discuter à travers le socker UNIX

* récupérer la liste des conteneurs

  

#### On vas récupérer les informations qui se trouvent dans le fichier que possède dockerd. ces informations seont récupérer en HTTP de meme sorte que docker communique avec dockerd via HTTP.

  

* Pour cela nous allons utiliser la fonction GET et CURL

  

>[root@localhost ~]# docker run -d alpine sleep 9999

[root@localhost ~]# docker ps

CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES

59369588999b alpine "sleep 99999" 4 seconds ago Up 2 seconds keen_chandrasekhar

  
  

>[root@localhost ~]# curl -X GET http://localhost:3333/containers/json

[{

"Id": "59369588999b8b9e3c7ac2a9c33a3759a37989e983c8ac018c643d5dc84e6c5c",

"Names": [

"/keen_chandrasekhar"],

"Image": "alpine",

"ImageID": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",

"Command": "sleep 99999",

"Created": 1579014948,

"Ports": [],

"Labels": {},

"State": "running",

"Status": "Up 6 seconds",

"HostConfig": {

"NetworkMode": "default"

},

"NetworkSettings": {

"Networks": {

"bridge": {

"IPAMConfig": null,

"Links": null,

"Aliases": null,

"NetworkID": "cf37432f67ca046739ad60577c3b6ec83bf39073312230f372c53ccc04dfb9c7",

"EndpointID": "47db1e2f61fba0ed63f2c1ad17be884cfe719531526cfb447347803ac0592e6f",

"Gateway": "172.17.0.1",

"IPAddress": "172.17.0.2",

"IPPrefixLen": 16,

"IPv6Gateway": "",

"GlobalIPv6Address": "",

"GlobalIPv6PrefixLen": 0,

"MacAddress": "02:42:ac:11:00:02",

"DriverOpts": null

}}},

"Mounts": []}]

  

>[root@localhost ~]# curl --unix-socket /var/run/docker.sock http://docker/containers/json[{

"Id": "59369588999b8b9e3c7ac2a9c33a3759a37989e983c8ac018c643d5dc84e6c5c",

"Names": [

"/keen_chandrasekhar"

],

"Image": "alpine",

"ImageID": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",

"Command": "sleep 99999",

"Created": 1579014948,

"Ports": [],

"Labels": {},

"State": "running",

"Status": "Up 6 seconds",

"HostConfig": {

"NetworkMode": "default"

},"NetworkSettings": {

"Networks": {

"bridge": {

"IPAMConfig": null,

"Links": null,

"Aliases": null,

"NetworkID": "cf37432f67ca046739ad60577c3b6ec83bf39073312230f372c53ccc04dfb9c7",

"EndpointID": "47db1e2f61fba0ed63f2c1ad17be884cfe719531526cfb447347803ac0592e6f",

"Gateway": "172.17.0.1",

"IPAddress": "172.17.0.2",

"IPPrefixLen": 16,

"IPv6Gateway": "",

"GlobalIPv6Address": "",

"GlobalIPv6PrefixLen": 0,

"MacAddress": "02:42:ac:11:00:02",

"DriverOpts": null}}},

"Mounts": []}]

  

* récupérer la liste des images disponibles

  

>[{

"Containers": -1,

"Created": 1578608418,

"Id": "sha256:c7460dfcab502275e9c842588df406444069c00a48d9a995619c243079a4c2f7",

"Labels": {

"maintainer": "NGINX Docker Maintainers <docker-maint@nginx.com>},

"ParentId": "",

"RepoDigests":

"nginx@sha256:8aa7f6a9585d908a63e5e418dc5d14ae7467d2e36e1ab4f0d8f9d059a3d071ce"],

"RepoTags": [

"nginx:latest"],

"SharedSize": -1,

"Size": 126324348,

"VirtualSize": 126324348},{

"Containers": -1,

"Created": 1577215212,

"Id": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",

"Labels": null,

"ParentId": "",

"RepoDigests": [

"alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78",

"alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78"],

"RepoTags": [

"alpine:latest",

"alpine:latest"],

"SharedSize": -1,

"Size": 5591300,

"VirtualSize": 5591300

},{

"Containers": -1,

"Created": 1571907605,

"Id": "sha256:621496cef4f8cdf5a9b44cd538d4a23c7546b9b86bbb041f50032c5dd3c9c444",

"Labels": null,

"ParentId": "",

"RepoDigests": [

"byrnedo/alpine-curl@sha256:ead845a9d8e26a80afc280ee10e62066c732418da0e11228c4f3dbc1389636ef"],

"RepoTags": [

"byrnedo/alpine-curl:latest"],

"SharedSize": -1,

"Size": 6893026,

"VirtualSize": 6893026},{

"Containers": -1,

"Created": 1546306167,

"Id": "sha256:fce289e99eb9bca977dae136fbe2a82b6b7d4c372474c9235adc1741675f587e",

"Labels": null,

"ParentId": "",

"RepoDigests": [

"hello-world@sha256:d1668a9a1f5b42ed3f46b70b9cb7c88fd8bdc8a2d73509bb0041cf436018fbf5"],

"RepoTags": [

"hello-world:latest"],

"SharedSize": -1,

"Size": 1840,

"VirtualSize": 1840},{

"Containers": -1,

"Created": 1516317158,

"Id": "sha256:e06c3dbbfe239c6fca50b6ab6935b3122930fa2eea2136979e5b46ad77ecb685",

"Labels": null,

"ParentId": "",

"RepoDigests": [

"httpd@sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb"],

"RepoTags": [

"httpd:2.2"],

"SharedSize": -1,

"Size": 171293537,

"VirtualSize": 171293537}]

  

🌞 Trouver les namespaces utilisés par votre shell.

  

  

>[root@localhost ~]# ps -ef

UID PID PPID C STIME TTY TIME CMD

root 1 0 0 09:29 ? 00:00:01 /usr/lib/systemd/systemd --switched-root --system --deserialize 22

root 2 0 0 09:29 ? 00:00:00 [kthreadd]

[...]

root 1286 1 0 09:29 ? 00:00:00 /usr/libexec/postfix/master -w

postfix 1297 1286 0 09:29 ? 00:00:00 qmgr -l -t unix -u

root 1537 758 0 09:29 tty1 00:00:00 -bash

postfix 1562 1286 0 09:30 ? 00:00:00 pickup -l -t unix -u

root 1565 1108 0 09:42 ? 00:00:00 sshd: root@pts/0

root 1578 1565 0 09:42 pts/0 00:00:00 -bash

root 1619 2 0 09:44 ? 00:00:00 [kworker/1:0]

root 1656 762 0 09:51 ? 00:00:00 /sbin/dhclient -d -q -sf /usr/libexec/nm-dhcp-helper -pf /var/run/d

root 1695 1108 0 09:51 ? 00:00:00 sshd: root@pts/1

root 1699 1695 0 09:51 pts/1 00:00:00 -bash

root 1717 2 0 09:52 ? 00:00:00 [kworker/0:0]

root 1729 2 0 09:53 ? 00:00:00 [kworker/1:1]

root 1730 1699 0 09:54 pts/1 00:00:00 ps -ef

  
  

#### Lorsque je regarde les namespaces q'utilise le PID 1 (systemd) et le PID de l'interface de mon shell (PID 1699 (pts/1)) on se rends compte qu'elles utilisent les même namespace

  

>[root@localhost ~]# ls -al /proc/1/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 09:56 .

dr-xr-xr-x. 9 root root 0 26 janv. 18:22 ..

lrwxrwxrwx. 1 root root 0 27 janv. 09:56 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 09:56 mnt -> mnt:[4026531840]

lrwxrwxrwx. 1 root root 0 27 janv. 09:56 net -> net:[4026531956]

lrwxrwxrwx. 1 root root 0 27 janv. 09:56 pid -> pid:[4026531836]

lrwxrwxrwx. 1 root root 0 27 janv. 09:56 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 09:56 uts -> uts:[4026531838]

  

>[root@localhost ~]# ls -al /proc/1699/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 10:03 .

dr-xr-xr-x. 9 root root 0 27 janv. 09:51 ..

lrwxrwxrwx. 1 root root 0 27 janv. 10:03 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 10:03 mnt -> mnt:[4026531840]

lrwxrwxrwx. 1 root root 0 27 janv. 10:03 net -> net:[4026531956]

lrwxrwxrwx. 1 root root 0 27 janv. 10:03 pid -> pid:[4026531836]

lrwxrwxrwx. 1 root root 0 27 janv. 10:03 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 10:03 uts -> uts:[4026531838]

  

#### Mais si l'on créé un container et que l'on regarde ses namespace on s'appersoit que ce dernier n'as absolument pas les mêmes; il est donc isolé du reste de la machine comme nous pouvons le voir ci-dessous.

  

  

>[root@localhost ~]# sudo docker run -d alpine sleep 9999

c31eeb67b103442ce6269dd52994eb3aecda649dbc53c8ecb44d1834ad16c1bd[

  

>root@localhost ~]# docker ps

CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES

c31eeb67b103 alpine "sleep 9999" About a minute ago Up About a minute musing_moore

  

>[root@localhost ~]# docker exec -it c31 sh

/ # ps -ef

PID USER TIME COMMAND

1 root 0:00 sleep 9999

6 root 0:00 sh

11 root 0:00 ps -ef

/ # ls -al /proc/1/ns

total 0

dr-x--x--x 2 root root 0 Jan 27 09:08 .

dr-xr-xr-x 9 root root 0 Jan 27 09:03 ..

lrwxrwxrwx 1 root root 0 Jan 27 09:08 ipc -> ipc:[4026532503]

lrwxrwxrwx 1 root root 0 Jan 27 09:08 mnt -> mnt:[4026532501]

lrwxrwxrwx 1 root root 0 Jan 27 09:08 net -> net:[4026532506]

lrwxrwxrwx 1 root root 0 Jan 27 09:08 pid -> pid:[4026532504]

lrwxrwxrwx 1 root root 0 Jan 27 09:08 user -> user:[4026531837]

lrwxrwxrwx 1 root root 0 Jan 27 09:08 uts -> uts:[4026532502]

  

### B. `unshare`

  

  

🌞 Créer un pseudo-conteneur à la main en utilisant `unshare`

* lancer une commande `unshare`

*  `unshare` doit exécuter le processus `bash`

* ce processus doit utiliser des namespaces différents de votre hôte :

* réseau

* mount

* PID

* user

* prouver depuis votre `bash` isolé que ces namespaces sont bien mis en place

#### Pour commencer nous allons créé un container uniquement isolé du réseaux

  

>[root@localhost ~]# unshare -n /bin/bash

[root@localhost ~]# ip a

1: lo: LOOPBACK mtu 65536 qdisc noop state DOWN group default qlen 1000

link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00

  
  

#### Ce container est donc isolé via l'interface réseaux mais pas par les autres interfaces. Si l'on veux répondre à la consigne il nous faut isoler complètement l'utilisateur à travers les 4 arguments demandés

  

>[root@localhost ~]# unshare -n -p --fork --mount-proc=/proc /bin/bash

  

#### Une fois le container lancé il faut ouvrir un autre shell et se reconnecter a la machine car sur la 1ere interface le container tourne

  

#### Sur la 2nd interface nous allons GREP tous les processus qui "utilisent" bash, cela nous permettra de connaitre leur PID

  

>[root@localhost ~]# ps -ef | grep bash

root 1537 758 0 09:29 tty1 00:00:00 -bash

root 1578 1565 0 09:42 pts/0 00:00:00 -bash

root 1699 1695 0 09:51 pts/1 00:00:00 -bash

root 3603 1699 0 10:41 pts/1 00:00:00 /bin/bash

root 3629 3603 0 10:43 pts/1 00:00:00 unshare -n -p --fork /bin/bash

root 3631 3629 0 10:43 pts/1 00:00:00 /bin/bash

root 3776 3763 0 11:33 pts/1 00:00:00 unshare -n -p --fork --mount-proc=/proc /bin/bash

root 3778 3776 0 11:33 pts/1 00:00:00 /bin/bash

root 3793 3789 0 11:33 pts/2 00:00:00 -bash

root 3811 3793 0 11:33 pts/2 00:00:00 grep --color=auto bash

#### Nous savons donc que le container que l'on recherche est à un PID de 3778

  

  

>[root@localhost ~]# ls -al /proc/3778/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 11:34 .

dr-xr-xr-x. 9 root root 0 27 janv. 11:33 ..

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 mnt -> mnt:[4026532758]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 net -> net:[4026532761]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 pid -> pid:[4026532759]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 uts -> uts:[4026531838]

  

#### Nous cherchons le PID de notre interface.

  

>[root@localhost ~]# echo $$

3793

  

>[root@localhost ~]# ls -al /proc/3793/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 11:34 .

dr-xr-xr-x. 9 root root 0 27 janv. 11:33 ..

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 mnt -> mnt:[4026531840]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 net -> net:[4026531956]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 pid -> pid:[4026531836]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 11:34 uts -> uts:[4026531838]

#### Lorsque l'on compare les PID's du container et celui de lamachine on se rends compte que seul l'ID du user et du domaine est le même, le fazit que l'ID du user soit le même est du à ma version de CentOS (7)

  

### C. Avec docker

  

🌞 Trouver dans quels namespaces ce conteneur s'exécute.

  

#### Afin de trouver le PID du docker que l'on vien de lancer on effectue une recherche ciblée

  

>[root@localhost ~]# ps -ef | grep sleep

root 1621 1604 0 15:32 ? 00:00:00 sleep 99999

root 1668 1571 0 15:35 pts/0 00:00:00 grep --color=auto sleep

  

#### Le PID du docker est donc 1621

  
  

>[root@localhost ~]# ls -al /proc/1621/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 15:32 .

dr-xr-xr-x. 9 root root 0 27 janv. 15:32 ..

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 ipc -> ipc:[4026532503]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 mnt -> mnt:[4026532501]

lrwxrwxrwx. 1 root root 0 27 janv. 15:32 net -> net:[4026532506]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 pid -> pid:[4026532504]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 uts -> uts:[4026532502]

  

>[root@localhost ~]# ls -al /proc/$$/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 15:41 .

dr-xr-xr-x. 9 root root 0 27 janv. 15:29 ..

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 mnt -> mnt:[4026531840]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 net -> net:[4026531956]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 pid -> pid:[4026531836]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 15:41 uts -> uts:[4026531838]

  

### D. `nsenter`

  

  

🌞 Utiliser `nsenter` pour rentrer dans les namespaces de votre conteneur en y exécutant un shell

  

* prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage

  

  

>[root@localhost ~]# ls /proc/$$/ns -al

total 0

dr-x--x--x. 2 root root 0 27 janv. 16:16 .

dr-xr-xr-x. 9 root root 0 27 janv. 16:16 ..

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 mnt -> mnt:[4026531840]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 net -> net:[4026531956]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 pid -> pid:[4026531836]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 uts -> uts:[4026531838]

  

>[root@localhost ~]# nsenter -t 1621 -n /bin/bash

[root@localhost ~]# ls -al /proc/$$/ns

total 0

dr-x--x--x. 2 root root 0 27 janv. 16:16 .

dr-xr-xr-x. 9 root root 0 27 janv. 16:16 ..

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 ipc -> ipc:[4026531839]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 mnt -> mnt:[4026531840]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 net -> net:[4026532506]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 pid -> pid:[4026531836]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 user -> user:[4026531837]

lrwxrwxrwx. 1 root root 0 27 janv. 16:16 uts -> uts:[4026531838]

  

### E. Et alors, les namespaces User ?

  

  

Nous pouvons changer de user en en créant un nouveau, ce dernier créera un nouvel environnement de travail dans /var/lib/docker

  

Dans notre cas le nouveau dossier s'appelle "100000.100000"

  

100000.100000 builder buildkit containers image network overlay2 plugins runtimes swarm tmp trust volumes

  

  

Si on se rends dans ce dossier :

  

[root@localhost ~]# ls /var/lib/docker/100000.100000/

  

builder buildkit containers image network overlay2 plugins runtimes swarm tmp trust volumes

  

On se rends compte qu'il y à les mêmes dossiers que dans le "cd.."

  

  

### F. Isolation réseau

  

  

Observer les opérations liées au réseau lors de l'exécution d'un conteneur

🌞 lancer un conteneur simple

  

On créé un container avce une "attribution de port"

  

[root@localhost ~]# docker run -d -p 8888:7777 debian sleep 9999

  

🌞 vérifier le réseau du conteneu

  

  

>[root@localhost ~]# docker inspect fa1 | grep "IPAddress"

"SecondaryIPAddresses": null,

"IPAddress": "172.17.0.3",

"IPAddress": "172.17.0.3",

🌞 vérifier le réseau sur l'hôte

  

>[root@localhost ~]# ip a | grep -e "docker0" -e "veth"

3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default

inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0

7: vethaa6845f@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default

9: vethadbf692@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default

  

2. Cgroups

  

  

A. Découverte manuelle

  

  

🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute

  

>[root@localhost ~]# docker exec -it 0a8 sh

cat /proc/1/cgroup

11:devices:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

10:cpuset:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

9:hugetlb:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

8:perf_event:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

7:pids:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

6:memory:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

5:freezer:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

4:net_prio,net_cls:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

3:cpuacct,cpu:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

2:blkio:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

1:name=systemd:/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba

  
  

  

#### A l'aide de la commande "systemd-cgtop" on affiche les services qui sont exécutés. On se rends compte que 3 instances docker sont actuelement en train de tourner.

  
  

>/system.slice/containerd.service 4 0.3 100.5M - -

/system.slice/docker.service 2 0.0 205.9M - -

/system.slice/tuned.service 1 0.0 13.3M - -

/system.slice/rsyslog.service 1 0.0 3.3M - -

/docker - - 204.0K - -

/docker/0a87c4545a33517f4f9152c6c8149134cda2005d846495ca49300bc079f613ba 1 - 76.0K - -

/docker/81dc18e10497d84675dc14193f2aa287191c833d1055aea3060dfe9a64f6199a 1 - 80.0K - -

/docker/a7f1696281d10101cd77c298023b44cb3dcbb044d7328279aed97e15c5c5b947 1 - 44.0K - -

/system.slice/NetworkManager.service 2 - 12.4M - -

/system.slice/auditd.service 1 - 3.0M - -

/system.slice/chronyd.service

  

🌞 Lancer un conteneur Docker et trouver
la mémoire RAM max qui lui est autorisée
le nombre de processus qu'il peut contenir
explorer un peu de vous-même ce qu'il est possible de faire avec des cgroups

  
#### On regarde la valeur de la variable "memory.max_usage_in_bytes" c'est elle qui définie la valeur max de ram qu'aura accès un docker 
>[root@localhost ~]# cat /sys/fs/cgroup/memory/docker/memory.max_usage_in_bytes
8859648

>[root@localhost ~]# cat /sys/fs/cgroup/pids/docker/pids.max
max

##### Dans notre cas les doskcers ont accès a 8859648bytes de ram
  
#### On lance un docker avec l'argument -m qui permet de limiter l'usage de la ram dans notre cas on la limite à 5Mo, on ajoute aussi l'argument pids limit qui bloque le nombre de services lancés à 10. comme indiqué ci dessus, par défaut il ny à pas de limite de pid car la valeur est à "MAX".
>[root@localhost ~]# docker run -d -m 5m --pids-limit 10 debian sleep 9999
3bb7afc5ff84663b28dd7ce344d0a2fec4a2da32e39b85796f4d95e39bbde5fb


>[root@localhost ~]# cat /sys/fs/cgroup/memory/docker/memory.max_usage_in_bytes
12922880

 3. Capabilities

A. Découverte manuelle

🌞 déterminer les capabilities actuellement utilisées par votre shell

  #### On affiche la valeur de "capsh" cette dernière nous informe de tous les arguments qu'elle comprends 
>[root@localhost ~]# capsh --print
Current: = cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36+ep Bounding set=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36
Securebits: 00/0x0/1'b0
secure-noroot: no (unlocked)
secure-no-suid-fixup: no (unlocked)
secure-keep-caps: no (unlocked)
uid=0(root)
gid=0(root)
groups=0(root)

  
  
  

🌞 Déterminer les capabilities du processus lancé par un conteneur Docker

#### On lance un container avec une limite de 10 pids 
>docker run -d -m 5m --pids-limit 10 debian sleep 9999


#### On cherche le PID du container ( 1633 )
>[root@localhost ~]#  ps -ef | grep sleep
100000     1686   1665  0 09:23 ?        00:00:00 sleep 9999
root       1739   1633  0 09:25 pts/1    00:00:00 grep --color=auto sleep

#### On regarde ce qui le statu du container 
>[root@localhost ~]# cat /proc/1633/status | grep Cap
CapInh: 0000000000000000
CapPrm: 0000001fffffffff
CapEff: 0000001fffffffff
CapBnd: 0000001fffffffff
CapAmb: 0000000000000000

#### Il y a désormais moins de services 
>[root@localhost ~]# capsh --decode=0000001fffffffff
0x0000001fffffffff=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36
  

[root@localhost ~]# yum install libcap-ng-utils