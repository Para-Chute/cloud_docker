
  

# Compte rendu Jeleff Clément B3B Infra & Réseaux

  

  

# Rendu TP3

## I. Lab setup
Mettez en place 3 machines :

CentOS7
#### stockage :
un disque supplémentaire (peu importe la taille, quelques Go suffiront) sur toutes les machines
sur node3, ajouter un troisième disque. Quelques Go aussi suffiront.


#### réseau

une carte accès internet
une carte accès à un LAN
hostname défini


#### SELinux désactivé
vos pouvez désactiver firewalld pour faciliter vos tests/déploiements

je vous conseille cependant de le laisser activé, et d'apprendre à correctement configurer les firewalls de vos machines


fichier /etc/hosts rempli avec les hostnames des autres noeuds



Dans un premier temps on install Vagrant sur la machine windows. Suite au redémarage de la machine, il nous fait configurer le fichier "Vagranfile" et de définir la configuration des machines shouaité.
Par exemple on peut définir qu'au démarage et/ou création de la machine cette dernière se mette a jour en exécutant d'elle meme les commandes :
sudo apt update
sudo apt install

ou installer toutes les fonctionnalitées néssésaires à un serveur LAMP. 

une fois le fichier coonfigurer avec la conf suivante 

[
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"


  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
  
  (1..3).each do |host|
    config.vm.define "vagrant#{host}" do |node|
      node.vm.network "private_network", ip: "192.168.33.1#{host}"
      node.vm.hostname = "node-#{host}"
      node.vm.provider :virtualbox do |vb|
        vb.name = "node-#{host}"
      end
    end
  end

  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
]