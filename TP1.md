
# Compte rendu Jeleff Clément B3B Infra & Réseaux

Rendu TP1

🌞##**Conteneur simple**##🌞

Dans un premier temps j'ai du connecter ma VM CentOS à un shell windows pour cela il faut:

-Lancer sa VM centOS (User: root, Mdp: root)
-Connaitre l’ip de sa VM CentOS à l’aide de la commande « ip a »
-Lancer un Powershell
>On se connecte a la VM avec la commande « ssh [Nom de la machine]@[IP de la machine] »

# Fonctionement de VIM (éditeur de fichier)

Acceder à un fichier :
vi [Nom du fichier]
Shift+a = passe en mode insert et dirige au bout de la ligne 
i = passer en mode insert
:w = sauvegarde le fichier
:q = quitte l'accès au fichier (:q!) forcer la fermeture  
echap : quite nimporte quel mode

Une fois tout cela terminé il faut installer : Alpine, Docker, yum, python3, nano avec la commande "sudo yum install [nom du service]"

## Activer le service Docker

docker ps : affiche les docker en cours d'exécution
systemctl : affiche tous les services instalés sur le poste
systemctl status [Nom du service] : affiche le statu du service en particulier
systemctl enable --now [Nom du service] : L'argument "enable permet d'activer le service a tous les démarages, le service now permet de l'activer tout de suite  

## 1. Lancer des conteneurs

Création d'un docker alpine :

[root@localhost ~]# sudo docker run -d alpine sleep 9999
a81ffb3ff311f470deda4f681d84317c10c3f279ece94b05bacbdeb94ad8b96e
[root@localhost ~]# docker -ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
a81ffb3ff311        alpine              "sleep 9999"        30 seconds ago      Up 28 seconds                           nostalgic_fermat

-> le docker c'est bien executé, pour vérifier que la commande s'est bien exécutée les prochaines fois,
on peut utiliser la commande "echo $?" 
Cette commande nous renvois la valeur de la variable "$"
cette variable prends la valeur de retour d'exécution de la commande précédente 
if $ > 0 la commande précédente s'est mal passée
if $ =! 0 la commande précédente s'est bien passée


-Afficher les conteneurs actuellement lancés
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

Il n'y a auccun conteneurs lancé car le précédent quel'on a lancé s'est fermé après s'etre exécuté 


Liste de tous les containers arrété:
[root@localhost ~]# docker container ls -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                         PORTS               NAMES
42536dc2831c        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       loving_mccarthy
860b9869e81a        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       adoring_feynman
cfbcec0a03c1        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       mystifying_gates
3147d0b5236d        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       dazzling_payne
d793e64ea7a0        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       keen_perlman
1c7ca77f01be        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       suspicious_goldstine

##  **Conteneur simple, mais vivant**  ##

Arreter un docker qui tourne en tache de fond : "docker rm -f [nom du container]

[root@localhost ~]# sudo docker run -d  alpine sleep 9999
1b1b31db58381d7ec4e505fd3437d43d6cf6e74307c1bb4234e4f52681854c50

##  **Manipulation du conteneur**  ##


- [root@localhost ~]# docker ps
>CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
1b1b31db5838        alpine              "sleep 9999"        25 seconds ago      Up 24 seconds                           wizardly_jones

Le container s'est bien lancé nous avons son id, son image, son nom, etc...

Pour entrer dans un container :
[root@localhost ~]# docker exec -it wizardly_jones sh
/ #   <- A cette endroit nous avons accès au container  
[root@localhost ~]#     

On vas donc prouver que le docker est bien isolé de la machine.

- /#Point de vue du docker:
>[root@localhost ~]# docker exec -it suspicious_bouman sh

- / # whoami
>root

- / # ps
>PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
    6 root      0:00 sh
   13 root      0:00 ps
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  17.0G      1.7G     15.3G  10% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                   745.2M         0    745.2M   0% /sys/fs/cgroup
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/mapper/centos-root
                         17.0G      1.7G     15.3G  10% /etc/resolv.conf
/dev/mapper/centos-root
                         17.0G      1.7G     15.3G  10% /etc/hostname
/dev/mapper/centos-root
                         17.0G      1.7G     15.3G  10% /etc/hosts
tmpfs                   745.2M         0    745.2M   0% /proc/asound
tmpfs                   745.2M         0    745.2M   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/timer_stats
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                   745.2M         0    745.2M   0% /proc/scsi
tmpfs                   745.2M         0    745.2M   0% /sys/firmware
- /# ip a
> 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
4: eth0@if5: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever


- #Point de vue de la machine:
- 
- / # [root@localhost ~]# whoami
>root (Le nom est le meme car j'ai appelé ma machine "root")
[root@localhost ~]# ps
   PID TTY          TIME CMD
  1394 pts/0    00:00:00 bash
  1903 pts/0    00:00:00 ps
[root@localhost ~]# df -h
Sys. de fichiers        Taille Utilisé Dispo Uti% Monté sur
devtmpfs                  734M       0  734M   0% /dev
tmpfs                     746M       0  746M   0% /dev/shm
tmpfs                     746M    9,7M  736M   2% /run
tmpfs                     746M       0  746M   0% /sys/fs/cgroup
/dev/mapper/centos-root    17G    1,7G   16G  10% /
/dev/sda1                1014M    142M  873M  14% /boot
tmpfs                     150M       0  150M   0% /run/user/0
overlay                    17G    1,7G   16G  10% /var/lib/docker/overlay2/8ebb8045c0ab5ca1f1c8fc75241ddf015f0ff517b7e2e85f3ad58530da8f982d/merged
- [root@localhost ~]# ip a
>1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:03:19:0e brd ff:ff:ff:ff:ff:ff
    inet 192.168.88.128/24 brd 192.168.88.255 scope global noprefixroute dynamic ens33
       valid_lft 1100sec preferred_lft 1100sec
    inet6 fe80::36bb:60b9:4c7b:5a64/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:3a:1c:98:29 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:3aff:fe1c:9829/64 scope link
       valid_lft forever preferred_lft forever
5: veth6ccbcd3@if4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 6a:d9:16:3f:71:72 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::68d9:16ff:fe3f:7172/64 scope link
       valid_lft forever preferred_lft forever
       
On constate bien que les deux "machines" sont distinctes 

🌞 Lancer un conteneur NGINX

[root@localhost ~]# docker run -p 80:80 -d nginx
8aff313cfae83f9f9d4ece8d8601f7b86fda817ab1e1e98ded9e674253dbc5b5

[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                   NAMES
8aff313cfae8        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute   0.0.0.0:32770->80/tcp   brave_ellis

- [root@localhost ~]# docker exec -it 8af sh
 - /#nginx -V
> nginx version: nginx/1.17.6
built by gcc 8.3.0 (Debian 8.3.0-6)
built with OpenSSL 1.1.1c  28 May 2019 (running with OpenSSL 1.1.1d  10 Sep 2019)
TLS SNI support enabled
configure arguments: --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fdebug-prefix-map=/data/builder/debuild/nginx-1.17.6/debian/debuild-base/nginx-1.17.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie'


- /# curl 0.0.0.0

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

Le conteneur NGINX fonctionne bien



## 2. Gestion d'images  ##


Création d'une image :


#[AFFICHER LE DOCKERFILE DE TOTO]


docker inspect [Nom du container] affiche toutes les infos du container

- Affiche les images présentes

>[root@localhost]# docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my_new_name         latest              28aca62d9d3b        51 minutes ago      6.97 MB
toto                latest              28aca62d9d3b        51 minutes ago      6.97 MB
docker.io/alpine    latest              965ea09ff2eb        7 weeks ago         5.55 MB


- Lancer le docker TOTO créé il y a 51min :
>[root@localhost]# docker run toto
bonjour



## Récupérer une image de Apache en version 2.2
Il nous est demandé de récupérer l'image Apache 2.2 or sur le docker apache on vois qu'il n'y à pas de version
2.2, la plus proche étant une 2.4

- On vas donc pull l'image apache:

>[root@localhost ~]# docker pull httpd:2.2
2.2: Pulling from library/httpd
f49cf87b52c1: Pull complete
24b1e09cbcb7: Pull complete
8a4e0d64e915: Pull complete
bcbe0eb4ca51: Pull complete
16e370c15d38: Pull complete
Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
Status: Downloaded newer image for httpd:2.2
docker.io/library/httpd:2.2

[root@localhost ~]# docker run -d -p 80:80 httpd:2.2


- Arreter un docker qui tourne en tache de fond : "docker rm -f [nom du container]

>[root@localhost ~]# docker rm -f fifty_leakey
fifty_leakey

- Le container est bien arrété



## Création Image

- Création du répertoir shouaité 

>[root@localhost /]# mkdir path
[root@localhost /]# cd path/
[root@localhost path]# mkdir to
[root@localhost path]# cd to
[root@localhost to]# mkdir dockerfile
[root@localhost to]# cd dockerfile/
[root@localhost dockerfile]# mkdir directory
[root@localhost dockerfile]# cd directory/

- Comme j'ai perdu le docker file je vais le reconstruire à partir du docker toto comme suivant:

[root@localhost ~]# docker build -t toto
"docker build" requires exactly 1 argument(s).
See 'docker build --help'.

Usage:  docker build [OPTIONS] PATH | URL | -

- Build an image from a Dockerfile

>[root@localhost ~]# docker build -t toto .
Sending build context to Docker daemon 13.31 kB
Step 1/4 : FROM alpine
 ---> 965ea09ff2eb
Step 2/4 : EXPOSE 8888/tcp
 ---> Using cache
 ---> 931a9f1077aa
Step 3/4 : RUN apk update
 ---> Using cache
 ---> 0bf326e3fd8a
Step 4/4 : CMD echo bonjour
 ---> Using cache
 ---> 28aca62d9d3b
Successfully built 28aca62d9d3b

- utiliser l'option `-v` de `docker run` pour partager des fichiers de l'hôte à l'aide du serveur web du conteneur
>[root@localhost ~]# docker run -p 8888:8888 -d 10b1edaaef96`
[root@localhost ~]# echo test > ./app/test.html
[root@localhost ~]# ll ./app
total 4
-rw-r--r--. 1 root root 7 Dec  9 20:19 test.html
[root@localhost ~]# docker run -p 8888:8888 -d -v "/root/app:/app" 10b1edaaef96
[root@localhost ~]# docker exec -it 603817e0a7b8 sh
/app # ls
test.html

## 3. Manipulation du démon docker
- 🌞 Modifier la configuration du démon Docker :

>toto
[root@localhost ~]# dockerd -H tcp://192.168.88.128
[root@localhost ~]# firewall-cmd --add-port=2375/tcp

>titi
[root@localhost ~]# docker -H tcp://192.168.88.128:2375 ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
398276e0d7a8        10b1eaccde39        "python3 -m http.ser…"   16 minutes ago      Exited (137) 5 minutes ago        brave_ellis

- Trouver l'emplacement par défaut /var/lib/docker

- Le déplacer dans un répertoire /data/docker créé à cet effet Dans le fichier /etc/docker/daemon.json :

>{
"data-root": "/data/docker"
}

- Modifier le score du démon Docker Dans le fichier /etc/docker/daemon.json :

>{
"data-root": "/data/docker",
"oom-score-adjust": 10
}



# 4. Docker-compose

- Write your own(cf. write_your_own pour le résultat attendu)
- Ecrire un docker-compose-v1.yml qui permet de :

>version: '3.7'
services:
  toto:
    build: .
    restart: on-failure
    ports:
      - "8888:8888"