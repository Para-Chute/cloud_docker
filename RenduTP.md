# Compte rendu Jeleff Clément B3B Invra & Réseaux

Rendu TP1

##**Conteneur simple**##

Dans un premier temps j'ai du connecter ma VM CentOS à un shell windows pour cela il faut:

Lancer sa VM centOS (User: root, Mdp: root)
Connaitre l’ip de sa VM CentOS à l’aide de la commande « ip a »
Lancer un Powershell
On se connecte a la VM avec la commande « ssh [Nom de la machine]@[IP de la machine] »

# Fonctionement de VIM (éditeur de fichier)

Acceder à un fichier :
vi [Nom du fichier]
Shift+a = passe en mode insert et dirige au bout de la ligne 
i = passer en mode insert
:w = sauvegarde le fichier
:q = quitte l'accès au fichier (:q!) forcer la fermeture  
echap : quite nimporte quel mode

Une fois tout cela terminé il faut installer : Alpine, Docker, yum, python3, nano avec la commande "sudo yum install [nom du service]"

#Activer le service Docker

docker ps : affiche les docker en cours d'exécution
systemctl : affiche tous les services instalés sur le poste
systemctl status [Nom du service] : affiche le statu du service en particulier
systemctl enable --now [Nom du service] : L'argument "enable permet d'activer le service a tous les démarages, le service now permet de l'activer tout de suite  

#On commence le TP

Création d'un docker alpine :

[root@localhost /]# docker run alpine
Unable to find image 'alpine:latest' locally
Trying to pull repository docker.io/library/alpine ...
latest: Pulling from docker.io/library/alpine
89d9c30c1d48: Pull complete                                                                                          Digest: sha256:c19173c5ada610a5989151111163d28a67368362762534d8a8121ce95cf2bd5a
Status: Downloaded newer image for docker.io/alpine:latest

-> le docker c'est bien executé, pour vérifier que la commande s'est bien exécutée les prochaines fois,
on peut utiliser la commande "echo $?" cette commande nous renvois la valeur de la variable "$"
cette variable prends la valeur de retour d'exécution de la commande précédente 
if $ > 0 la commande précédente s'est mal passée
if $ =! 0 la commande précédente s'est bien passée


-Afficher les conteneurs actuellement lancés
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

Il n'y a auccun conteneurs lancé car le précédent quel'on a lancé s'est fermé après s'etre exécuté 


Liste de tous les containers arrété:
[root@localhost ~]# docker container ls -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                         PORTS               NAMES
42536dc2831c        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       loving_mccarthy
860b9869e81a        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       adoring_feynman
cfbcec0a03c1        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       mystifying_gates
3147d0b5236d        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       dazzling_payne
d793e64ea7a0        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       keen_perlman
1c7ca77f01be        alpine              "/bin/sh"           About an hour ago   Exited (0) About an hour ago                       suspicious_goldstine

##  **Conteneur simple, mais vivant**  ##

Arreter un docker qui tourne en tache de fond : "docker rm -f [nom du container]

[root@localhost ~]# sudo docker run -d  alpine sleep 9999
1b1b31db58381d7ec4e505fd3437d43d6cf6e74307c1bb4234e4f52681854c50

##  **Manipulation du conteneur**  ##


[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
1b1b31db5838        alpine              "sleep 9999"        25 seconds ago      Up 24 seconds                           wizardly_jones

Le container s'est bien lancé nous avons son id, son image, son nom, etc...

Pour entrer dans un container :
[root@localhost ~]# docker exec -it wizardly_jones sh
/ #   <- A cette endroit nous avons accès au container  
[root@localhost ~]#     


Arreter un docker qui tourne en tache de fond : "docker rm -f [nom du container]

[root@localhost ~]# docker rm -f fifty_leakey
fifty_leakey

le container est bien arrété

## 2. Gestion d'images  ##


Création d'une image :


#[AFFICHER LE DOCKERFILE DE TOTO]


docker inspect [Nom du container] affiche toutes les infos du container

-Affiche les images présentes

[root@localhost]# docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my_new_name         latest              28aca62d9d3b        51 minutes ago      6.97 MB
toto                latest              28aca62d9d3b        51 minutes ago      6.97 MB
docker.io/alpine    latest              965ea09ff2eb        7 weeks ago         5.55 MB

Lance le docker TOTO créé il y a 51min

[root@localhost]# docker run toto
bonjour
[root@localhost ~]#


#récupérer une image de Apache en version 2.2
Il nous est demandé de récupérer l'image Apache 2.2 or sur le docker apache on vois qu'il n'y à pas de version
2.2, la plus proche étant une 2.4

on vas donc pull l'image apache:

[root@localhost ~]# docker pull apache:2.4
Trying to pull repository docker.io/library/apache ...
repository docker.io/apache not found: does not exist or no pull access



##Création Image

Création du répertoir shouaité 

[root@localhost /]# mkdir path
[root@localhost /]# cd path/
[root@localhost path]# mkdir to
[root@localhost path]# cd to
[root@localhost to]# mkdir dockerfile
[root@localhost to]# cd dockerfile/
[root@localhost dockerfile]# mkdir directory
[root@localhost dockerfile]# cd directory/



Comme j'ai perdu le docker file je vais le reconstruire a partir du docker toto comme suivant :

[root@localhost ~]# docker build -t toto
"docker build" requires exactly 1 argument(s).
See 'docker build --help'.

Usage:  docker build [OPTIONS] PATH | URL | -

Build an image from a Dockerfile
[root@localhost ~]# docker build -t toto .
Sending build context to Docker daemon 13.31 kB
Step 1/4 : FROM alpine
 ---> 965ea09ff2eb
Step 2/4 : EXPOSE 8888/tcp
 ---> Using cache
 ---> 931a9f1077aa
Step 3/4 : RUN apk update
 ---> Using cache
 ---> 0bf326e3fd8a
Step 4/4 : CMD echo bonjour
 ---> Using cache
 ---> 28aca62d9d3b
Successfully built 28aca62d9d3b
